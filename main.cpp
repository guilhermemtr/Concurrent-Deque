#include <iostream>
#include <assert.h>
#include "deque.hpp"

using namespace std;

void
test1()
{
  deque<int> d(8);
  cout << "constructed deque" << endl;
  d.push_front(1);
  cout << "pushed front int" << endl;
  assert(d.pop_front() == 1);
  cout << "poped front int" << endl;
  d.push_back(2);
  cout << "pushed back int" << endl;
  assert(d.pop_back() == 2);
  cout << "poped back int" << endl;
}

int
main()
{
  test1();
  cout << "test1 ended" << endl;
  

  return 0;
}
